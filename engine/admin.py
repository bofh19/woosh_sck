from django.contrib import admin
from engine.models import UserData, Relation, UserObtainedFrom, UserAdditionalFields, UserMessage


admin.site.register(UserData)
admin.site.register(Relation)
admin.site.register(UserAdditionalFields)
admin.site.register(UserObtainedFrom)
admin.site.register(UserMessage)