from django.db import models
from django.contrib.auth.models import User
import string
import random
import os
import uuid

KEY_DIGITS = 16


def get_file_path(instance, filename):
    ext = filename.split('.')[-1]
    filename = "%s-%s.%s" % (instance.user.username, uuid.uuid4(), ext)
    return os.path.join(instance.directory_string_var, filename)


class UserData(models.Model):
    user = models.OneToOneField(User, on_delete=models.SET_NULL, related_name="User", null=True, blank=True)
    key = models.CharField(max_length=16, blank=True, default='', db_index=True)

    creation_date = models.DateTimeField(verbose_name="Creation date", auto_now_add=True)
    modified_date = models.DateTimeField(verbose_name="Modified date", auto_now=True)

    enabled = models.BooleanField(default=True)
    claimed = models.BooleanField(default=False)

    additional_name = models.CharField(max_length=255, blank=True, null=True)
    full_name = models.CharField(max_length=255, blank=True, null=True)
    type = models.IntegerField(default=0)
    display_name = models.CharField(max_length=255, blank=True, null=True)

    invited = models.BooleanField(default=False)

    contacts_fetched = models.IntegerField(default=0)

    profile_pic = models.ImageField(upload_to=get_file_path, null=True, blank=True)
    directory_string_var = 'profile_images/user_uploaded/'

    user_status = models.TextField(default='')

    @staticmethod
    def get_google_user_type():
        return 1

    def can_be_invited(self):
        if not self.claimed and not self.invited:
            return True
        else:
            return False

    def __str__(self):
        return u"%s for %s " % (self.key, self.user)

    def save(self, *args, **kwargs):
        if not self.key:
            self.key = self.generate_key()
        return super(UserData, self).save(*args, **kwargs)

    def to_json(self):
        if self.display_name:
            name = self.display_name
        elif self.full_name:
            name = self.full_name
        elif self.additional_name:
            name = self.additional_name
        else:
            name = self.user.first_name
        return {'id': self.id, 'email': self.user.username, 'display_name': name, 'claimed': self.claimed,
                'invited': self.invited, 'bitmapUrl': self.profile_pic.url if self.profile_pic else None,
                'user_status': self.user_status,
                'additional_fields': [x.to_json() for x in self.useradditionalfields_set.all()]}

    @staticmethod
    def generate_key():
        return ''.join(random.SystemRandom().choice(string.ascii_uppercase + string.digits) for _ in range(KEY_DIGITS))


class UserObtainedFrom(models.Model):
    which_user = models.ForeignKey(UserData, related_name='ref_%(class)s_which_user')
    obtained_from = models.ForeignKey(UserData, related_name='ref_%(class)s_obtained_from')

    def __str__(self):
        return str(self.which_user) + '---' + str(self.obtained_from)

    class Meta:
        unique_together = (("which_user", "obtained_from"),)


class UserAdditionalFields(models.Model):
    user = models.ForeignKey(UserData)
    field_name = models.CharField(max_length=255)
    field_data = models.CharField(max_length=255)

    def __str__(self):
        return str(self.user) + '/' + self.field_name + ' - ' + self.field_data

    class Meta:
        unique_together = (("field_name", "field_data", "user"),)

    def to_json(self):
        if self.field_name == 'other_addr':
            field_name = 'Additional Login'
        else:
            field_name = self.field_name
        return {'user_id': self.user.id, 'field_name': field_name, 'field_data': self.field_data}


class UserMessage(models.Model):
    from_user = models.ForeignKey(UserData, related_name='ref_%(class)s_from_user')
    to_user = models.ForeignKey(UserData, related_name='ref_%(class)s_to_user')
    message = models.TextField(null=True, blank=True)
    hkey = models.TextField(max_length=255, default="UNKNOWN")
    received = models.BooleanField(default=False)
    deleted = models.BooleanField(default=False)

    def __str__(self):
        return str(self.from_user) + ' -> ' + str(self.to_user) + ' r: ' + str(self.received) + ' d: ' + str(
            self.deleted)


def create_api_key(sender, **kwargs):
    if kwargs.get('created') is True:
        UserData.objects.create(user=kwargs.get('instance'))


models.signals.post_save.connect(create_api_key, sender=User)


class Relation(models.Model):
    user1 = models.ForeignKey(UserData, related_name='user1')
    user2 = models.ForeignKey(UserData, related_name='user2')

    USER_CHOICES = ((0, 0), (1, 1), (2, 2), (3, 3))
    # 3 is for future
    # 0 is no idea who sent the request
    # 1 is user1
    # 2 is user2

    request_from_user = models.IntegerField(choices=USER_CHOICES, default=0, verbose_name="request_from_user")
    request_accepted_by_user = models.BooleanField(default=False)
    ignored_by_user1 = models.IntegerField(default=False, verbose_name="ignored_by_user1")
    ignored_by_user2 = models.BooleanField(default=False, verbose_name="ignored_by_user2")
    are_friends = models.BooleanField(default=False, verbose_name="are these 2 friends ?")

    def __str__(self):
        return u"%s and %s" % (self.user1, self.user2)

    class Meta:
        unique_together = ('user1', 'user2',)