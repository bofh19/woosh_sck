from engine.server.key_code import KeyCode
from django.test import TestCase
from django.contrib.auth.models import User
from engine.server.interface import validate_key, get_user_data_from_user, make_friends
from engine.server.interface import force_make_friends, send_request, accept_request, make_user_claimed
from engine.server.interface import get_friends_of_user, get_requests_pending_to_user, are_friends
from engine.server.interface import ignore_on_user_by_user, un_ignore_on_user_by_user, get_ignored_users_by


# class KeyCodeTests(TestCase):
#     def testKeyCodeLengthTest(self):
#         """
#         length should return true
#         """
#         # base_dicts = ["__doc__", "__init__", "__module__", "is_valid_key_code", "__dict__"]
#         # res = set(dir(KeyCode)) - set(base_dicts)
#         # res = list(res)
#         members = getattr(KeyCode, 'array')
#         self.assertEqual(len(members), len(set(members)), "All KeyCode's Must be unique")
#         for member in members:
#             true_val = len(member)
#             if true_val != 4:
#                 self.assertEqual(True, False, "Message length of %s mush be 4 exactly" % member)
#         return self.assertEqual(True, True)


class UserDataEngineTests(TestCase):
    def testEngineValidKeys(self):
        future_user = User.objects.create_user("test_user", "test_user@test.com", "test_user")
        future_user.save()
        user_from_user = get_user_data_from_user(future_user)
        if future_user is None:
            self.assertEqual(True, False, "Failed to get user for an user that was just created")
            return
        key = user_from_user.key
        user_from_validate_key = validate_key(key)
        self.assertEqual(user_from_user.id == user_from_validate_key.id, True, "Keys are not equal")


class UserRelationTests(TestCase):
    def testUserRelationsAll(self):
        f_user_1 = User.objects.create_user("f_user_1", "test_user@test.com", "test_user")
        f_user_1.save()
        f_user_1 = get_user_data_from_user(f_user_1)
        make_user_claimed(f_user_1)
        f_user_2 = User.objects.create_user("f_user_2", "test_user@test.com", "test_user")
        f_user_2.save()
        f_user_2 = get_user_data_from_user(f_user_2)
        make_user_claimed(f_user_2)
        f_user_3 = User.objects.create_user("f_user_3", "test_user@test.com", "test_user")
        f_user_3.save()
        f_user_3 = get_user_data_from_user(f_user_3)
        make_user_claimed(f_user_3)
        print("created 3 users")
        self.assertEqual(are_friends(f_user_1, f_user_2), False, "failed f_user_1,f_user_2 cannot be friends yet")
        self.assertEqual(are_friends(f_user_2, f_user_1), False, "failed f_user_2,f_user_1 cannot be friends yet")
        self.assertEqual(are_friends(f_user_3, f_user_1), False, "failed f_user_3,f_user_1 cannot be friends yet")

        send_request(f_user_1, f_user_2)
        self.assertEqual(are_friends(f_user_1, f_user_2), False,
                         "failed f_user_1,f_user_2 cannot be friends yet request just sent")

        accept_request(f_user_2, f_user_1)
        self.assertEqual(are_friends(f_user_1, f_user_2), True, "f_user_1,f_user_2 have to be friends now")

        ignore_on_user_by_user(on_user=f_user_1, by_user=f_user_3)
        self.assertEqual(are_friends(f_user_3, f_user_1), False,
                         "f_user_3 and f_user_1 cannot be friends ignored just now")

        users_list = get_ignored_users_by(f_user_3)
        found_count = 0
        for u in users_list:
            if f_user_1.user.id == u.user.id:
                found_count += 1
        self.assertGreaterEqual(found_count, 1, "f_user_3 ignores list must have f_user_1")
        make_friends(f_user_1, f_user_3)
        self.assertEqual(are_friends(f_user_3, f_user_1), False,
                         "f_user_3 and f_user_1 cannot be friends yet ")
        un_ignore_on_user_by_user(on_user=f_user_1, by_user=f_user_3)
        make_friends(f_user_1, f_user_3)
        self.assertEqual(are_friends(f_user_3, f_user_1), True,
                         "f_user_3 and f_user_1 have to be friends un_ignored just now")

        users_list = get_ignored_users_by(f_user_3)
        found_count = 0
        for u in users_list:
            if f_user_1.user.id == u.user.id:
                found_count += 1
        self.assertEqual(found_count, 0, "f_user_3 ignores list must not have f_user_1")

        ignore_on_user_by_user(on_user=f_user_1, by_user=f_user_3)
        self.assertEqual(are_friends(f_user_3, f_user_1), False,
                         "f_user_3 and f_user_1 cannot be friends ignored just now again")

        force_make_friends(f_user_1, f_user_3)
        self.assertEqual(are_friends(f_user_3, f_user_1), True,
                         "f_user_3 and f_user_1 must be friends force_made_just_now")

        ignore_on_user_by_user(on_user=f_user_3, by_user=f_user_1)
        self.assertEqual(are_friends(f_user_3, f_user_1), False,
                         "f_user_1 and f_user_3 cannot be friends ignored just now")
        un_ignore_on_user_by_user(on_user=f_user_1, by_user=f_user_3)
        self.assertEqual(are_friends(f_user_3, f_user_1), False,
                         "f_user_1 and f_user_3 cannot be friends yet")

        un_ignore_on_user_by_user(on_user=f_user_3, by_user=f_user_1)
        self.assertEqual(are_friends(f_user_3, f_user_1), True,
                         "f_user_1 and f_user_3 must be friends un ignored just now")