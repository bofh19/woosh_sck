__author__ = 'saikrishnak'

from django.conf.urls import patterns, url

urlpatterns = patterns('engine.views',
                       url(r'auth/1/$', 'step1_oauth', name='step1_oauth'),
                       url(r'auth/2/$', 'step2_oauth', name='step2_oauth'),
                       # url(r'user/$', 'get_user_info', name='get_user_info'),
)