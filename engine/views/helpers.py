__author__ = 'saikrishnak'

from django.http import HttpResponse
import json

mime_type = 'application/json'


def get_resp_with_status(message, error=None):
	resp = {}
	resp['request_status'] = message
	if (error is not None):
		resp['request_status'] = message + " " + str(error)
	resp['request_status_bool'] = False
	return HttpResponse(json.dumps(resp), mime_type)
