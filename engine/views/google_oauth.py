from collections import OrderedDict
from django.core.exceptions import ObjectDoesNotExist
import xmltodict

__author__ = 'saikrishnak'

from django.shortcuts import render_to_response
from django.template import RequestContext
from django.http import HttpResponse
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt
from engine.models import UserData, UserAdditionalFields, UserObtainedFrom

import urllib.request, urllib.parse, urllib.error
import urllib.request, urllib.error, urllib.parse
import json

from engine.server.interface import make_friends

G_C_ID = '405284180877-sepfua7de171m21oguu77rkgh8u7qtai.apps.googleusercontent.com'
G_C_SECRET = 'jDbsQnmK08kPB_xSmg0XhRYd'

# token_url = 'https://accounts.google.com/o/oauth2/token'
# redirect_url = 'http://rmrf.w4rlock.in/api/auth/2/'
token_url = 'https://www.googleapis.com/oauth2/v3/token'
redirect_url = "http://127.0.0.1:8000/api/auth/2/"
USER_INFO_URL = 'https://www.googleapis.com/oauth2/v2/userinfo'
USER_CONTACTS_URL = ''

mime_type = 'application/json'


def step1_oauth(request):
    scopes = "https://www.googleapis.com/auth/userinfo.profile+https://www.googleapis.com/auth/userinfo.email+https://www.google.com/m8/feeds/"
    return render_to_response(
        "oauth_test.html", {'id': G_C_ID, 'redirect_url': redirect_url, 'scope': scopes},
        context_instance=RequestContext(request))


# @csrf_exempt
# def get_user_info(request):
# try:
# user = is_authenticated(request)
# except Exception, e:
# print e
# return get_resp_with_status('failed')
# try:
# user_info = get_user_data(user)
# return HttpResponse(json.dumps(user_info), mimetype)
# except Exception, e:
# print e
#         return get_resp_with_status('failed')


@csrf_exempt
def step2_oauth(request):
    api_key = 'failed'
    key_id = 'failed'
    if request.GET:
        access_token = request.GET.get('access_token')
        code = request.GET.get('code')
        if access_token is None and code is None:
            api_key = 'failed - 2'

        if access_token is not None:
            # try:
            api_key, key_id = get_user_info_from_access_token(access_token)
            print(api_key, key_id)
            # except Exception as e:
            #     api_key = 'failed - 4 ' + str(e)

        elif code is not None:
            try:
                api_key, key_id = (get_user_info_from_code(code))
                print(api_key, key_id)
            except Exception as e:
                api_key = 'failed - 5 ' + str(e)
        else:
            api_key = 'failed - 1'

    res = {'api_key': api_key, 'id': key_id}
    return HttpResponse(json.dumps(res), mime_type)


def get_user_info_from_code(code):
    access_token = get_user_access_token_from_code(code)
    print(access_token)
    return get_user_info_from_access_token(access_token)


def get_user_info_from_access_token(access_token):
    query_args_2 = {
        'Host': 'www.googleapis.com',
        'Content-length': 0,
        'Authorization': 'Bearer ' + access_token,
    }
    print(query_args_2)
    data2 = urllib.parse.urlencode(query_args_2)
    data2 = data2.encode('utf-8')

    url = USER_INFO_URL  # + '?access_token=' + access_token
    request = urllib.request.Request(url, headers=query_args_2)
    conn = urllib.request.urlopen(request)
    data = conn.read()
    data = json.loads(data.decode('utf-8'))
    print(data)
    user_email = data['email']
    api_key = ''
    try:
        user, created = User.objects.get_or_create(username=user_email)
        if created:
            user.first_name = data['given_name']
            user.last_name = data['family_name']
            user.save()
    except Exception as e:
        print(e)
    conn.close()
    api_key = UserData.objects.get(user=user.id)
    api_key.claimed = True
    api_key.enabled = True
    api_key.save()
    if api_key.contacts_fetched <= 0:
        curl = "https://www.google.com/m8/feeds/contacts/default/full/?max-results=1000"
        print("fetching %s" % curl)
        query_args_2 = {
            'Host': 'www.google.com',
            'Content-length': 0,
            'Authorization': 'Bearer ' + access_token,
            'GData-Version': '3.0'
        }
        print(query_args_2)
        request = urllib.request.Request(curl, headers=query_args_2)
        conn = urllib.request.urlopen(request)
        data = conn.read()
        print("url fetched")
        print("-----api_key-----2:", api_key)
        save_google_contacts_data_to_db(data, api_key)
        conn.close()
        api_key.contacts_fetched += 1
        api_key.save()
    return api_key.key, api_key.id


def get_user_access_token_from_code(code):
    query_args = {
        'code': code,
        'redirect_uri': redirect_url,
        "redirect_uris": ["urn:ietf:wg:oauth:2.0:oob", "oob"],
        'client_id': G_C_ID,
        'scope': '',
        'client_secret': G_C_SECRET,
        'grant_type': 'authorization_code'
    }

    data = urllib.parse.urlencode(query_args)
    data = data.encode('utf-8')
    conn = urllib.request.urlopen(token_url, data)
    d = conn.read()
    print(d)
    d = json.loads(d.decode('utf-8'))
    token = d['access_token']
    conn.close()

    return token


import xml.etree.ElementTree as ET


def save_google_contacts_data_to_db(data, from_user):
    root = ET.fromstring(data)
    for each_entry in root.findall('{http://www.w3.org/2005/Atom}entry'):
        name = each_entry.find('{http://schemas.google.com/g/2005}name')
        emails = each_entry.findall('{http://schemas.google.com/g/2005}email')

        if name is not None:
            additional_name = name.find('{http://schemas.google.com/g/2005}additionalName')
            if additional_name is not None:
                additional_name = additional_name.text
            family_name = name.find('{http://schemas.google.com/g/2005}familyName')
            if family_name is not None:
                family_name = family_name.text
            full_name = name.find('{http://schemas.google.com/g/2005}fullName')
            if full_name is not None:
                full_name = full_name.text
            given_name = name.find('{http://schemas.google.com/g/2005}givenName')
            if given_name is not None:
                given_name = given_name.text
        else:
            additional_name = None
            family_name = None
            full_name = None
            given_name = None

        primary_email = None
        other_addrs = []
        for email in emails:
            if 'primary' in email.attrib.keys():
                if email.attrib['primary'] == 'true':
                    primary_email = email.attrib['address']
                else:
                    other_addrs.append(email.attrib['address'])
            else:
                other_addrs.append(email.attrib['address'])

        if primary_email is not None:
            user, created = User.objects.get_or_create(username=primary_email)
            if created:
                if given_name is not None:
                    user.first_name = given_name
                elif full_name is not None:
                    user.first_name = full_name
                else:
                    user.first_name = 'UNKNOWN'
                if family_name is not None:
                    user.last_name = family_name
                else:
                    user.last_name = 'UNKNOWN'
                user.save()

            ud = UserData.objects.get(user=user)
            # print("----ud-----", ud)
            # print("----from_user------", from_user)
            user_obtained, created = UserObtainedFrom.objects.get_or_create(which_user=ud, obtained_from=from_user)
            user_obtained.save()

            ud.additional_name = additional_name
            ud.full_name = full_name
            ud.type = UserData.get_google_user_type()
            ud.save()
            try:
                other_user_obtained = UserObtainedFrom.objects.get(obtained_from=ud, which_user=from_user)
                # this means both of them have each of them in contacts make them friends auto
                if ud.id != from_user.id:
                    make_friends(ud, from_user)
            except ObjectDoesNotExist:
                pass

            for other_addr in other_addrs:
                uaf, created = UserAdditionalFields.objects.get_or_create(user=ud, field_name="other_addr",
                                                                          field_data=other_addr)
                uaf.save()