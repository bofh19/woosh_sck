# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
from django.conf import settings
import django.db.models.deletion
import engine.models


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Relation',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('request_from_user', models.IntegerField(verbose_name='request_from_user', default=0, choices=[(0, 0), (1, 1), (2, 2), (3, 3)])),
                ('request_accepted_by_user', models.BooleanField(default=False)),
                ('ignored_by_user1', models.IntegerField(verbose_name='ignored_by_user1', default=False)),
                ('ignored_by_user2', models.BooleanField(verbose_name='ignored_by_user2', default=False)),
                ('are_friends', models.BooleanField(verbose_name='are these 2 friends ?', default=False)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserAdditionalFields',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('field_name', models.CharField(max_length=255)),
                ('field_data', models.CharField(max_length=255)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserData',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('key', models.CharField(db_index=True, default='', max_length=16, blank=True)),
                ('creation_date', models.DateTimeField(verbose_name='Creation date', auto_now_add=True)),
                ('modified_date', models.DateTimeField(verbose_name='Modified date', auto_now=True)),
                ('enabled', models.BooleanField(default=True)),
                ('claimed', models.BooleanField(default=False)),
                ('additional_name', models.CharField(null=True, max_length=255, blank=True)),
                ('full_name', models.CharField(null=True, max_length=255, blank=True)),
                ('type', models.IntegerField(default=0)),
                ('display_name', models.CharField(null=True, max_length=255, blank=True)),
                ('invited', models.BooleanField(default=False)),
                ('contacts_fetched', models.IntegerField(default=0)),
                ('profile_pic', models.ImageField(null=True, upload_to=engine.models.get_file_path, blank=True)),
                ('user', models.OneToOneField(related_name='User', on_delete=django.db.models.deletion.SET_NULL, null=True, blank=True, to=settings.AUTH_USER_MODEL)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserMessage',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message', models.TextField(null=True, blank=True)),
                ('hkey', models.TextField(default='UNKNOWN', max_length=255)),
                ('received', models.BooleanField(default=False)),
                ('deleted', models.BooleanField(default=False)),
                ('from_user', models.ForeignKey(related_name='ref_usermessage_from_user', to='engine.UserData')),
                ('to_user', models.ForeignKey(related_name='ref_usermessage_to_user', to='engine.UserData')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='UserObtainedFrom',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('obtained_from', models.ForeignKey(related_name='ref_userobtainedfrom_obtained_from', to='engine.UserData')),
                ('which_user', models.ForeignKey(related_name='ref_userobtainedfrom_which_user', to='engine.UserData')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AlterUniqueTogether(
            name='userobtainedfrom',
            unique_together=set([('which_user', 'obtained_from')]),
        ),
        migrations.AddField(
            model_name='useradditionalfields',
            name='user',
            field=models.ForeignKey(to='engine.UserData'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='useradditionalfields',
            unique_together=set([('field_name', 'field_data', 'user')]),
        ),
        migrations.AddField(
            model_name='relation',
            name='user1',
            field=models.ForeignKey(related_name='user1', to='engine.UserData'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='relation',
            name='user2',
            field=models.ForeignKey(related_name='user2', to='engine.UserData'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='relation',
            unique_together=set([('user1', 'user2')]),
        ),
    ]
