# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('engine', '0002_userdata_status'),
    ]

    operations = [
        migrations.RenameField(
            model_name='userdata',
            old_name='status',
            new_name='user_status',
        ),
    ]
