__author__ = 'saikrishnak'


__author__ = 'saikrishnak'

import threading
import socketserver
from engine.server.interface import validate_key, get_user_data_from_user, make_friends
from engine.server.interface import force_make_friends, send_request, accept_request, make_user_claimed
from engine.server.interface import get_friends_of_user, get_requests_pending_to_user, are_friends
from engine.server.interface import ignore_on_user_by_user, un_ignore_on_user_by_user, get_ignored_users_by
from engine.server.interface import get_user_data_from_unique_id, get_user_data_from_key
from engine.server.key_code import KeyCode
from engine.models import UserData, UserMessage
import json

host, port = "0.0.0.0", 6070
import socket
import threading
import select
import sys


active_sockets = {}
selectable_sockets = set([])


def validate_socket(ac_socket):
    key = ac_socket.recv(16)
    if not validate_key(key):
        return False, key
    else:
        return True, key


DATA_LENGTH_FIELD_LENGTH = 10


def prepare_socket_message(key_code, data):

    data_length = str(len(data))
    if len(data_length) >= 10:
        return key_code + data_length + data
    else:
        for i in range(len(data_length), 10):
            data_length = "0" + data_length
        return key_code + data_length + data


def prepare_final_msg(key, key_codes):
    key_codes_length = str(len(key_codes))
    if len(key_codes_length) == 1:
        key_codes_length = "0" + key_codes_length
    if type(key) == bytes:
        key = key.decode('utf-8')
    return bytes((str(key) + key_codes_length + ''.join(key_codes)), 'UTF-8')  # .encode('utf-8')


class InvalidKeyCodeException(Exception):
    def __init__(self, message):
        super(InvalidKeyCodeException, self).__init__(message)


class InvalidKeyDataLengthException(Exception):
    def __init__(self, message):
        super(InvalidKeyDataLengthException, self).__init__(message)


def read_one_key_code_and_data_from_socket(ac_socket):
    current_key_code = ac_socket.recv(4)
    # print(type(current_key_code),current_key_code)
    current_key_code = current_key_code.decode('utf-8')
    if not KeyCode.is_valid_key_code(current_key_code):
        print("invalid key code %s " % current_key_code)
        raise InvalidKeyCodeException("This is %s not a valid key" % current_key_code)
    try:
        size_of_this_key_code = int(ac_socket.recv(10))
    except Exception:
        print('invalid length for key')
        raise InvalidKeyDataLengthException("Invalid Length for %s key code given" % current_key_code)
    data = ""
    data_fetched = 0
    while data_fetched < size_of_this_key_code:
        data_fetched += 1
        data += (ac_socket.recv(1)).decode('utf-8')

    return current_key_code, data


def offline_message_process(user, message):
    pass


def process_socket(ac_socket, key):
    """
    assumes socket validation has occurred.
    removes from selectable sockets
    and adds it after processing has been done
    :param ac_socket:
    :return:
    """
    global selectable_sockets
    try:
        selectable_sockets.remove()
    except Exception:
        print("unable to remove socket from selectable_sockets. no idea why")
    try:
        no_of_codes = int(ac_socket.recv(2))
        print("no of codes", no_of_codes)
    except Exception:
        selectable_sockets.add(ac_socket)
        raise Exception("Invalid no of Key Codes")

    codes_read = 0
    for each_code in range(0, no_of_codes):
        # no equals as each_code will be 0 and codes_read will be 1 when reading 1st code
        if codes_read > each_code:
            break
        try:
            current_key_code, data = read_one_key_code_and_data_from_socket(ac_socket)
            print(current_key_code, data)
            codes_read += 1
        except InvalidKeyCodeException as e:
            print(e)
            ac_socket.sendall(prepare_final_msg(key, [prepare_socket_message(KeyCode.invalid, '1')]))
            break
        except InvalidKeyDataLengthException as e:
            ac_socket.sendall(prepare_final_msg(key, [prepare_socket_message(KeyCode.invalid, '2')]))
            break

        if current_key_code == KeyCode.ping:
            print('sending %s in reply' % KeyCode.pong)
            ac_socket.sendall(prepare_final_msg(key, [prepare_socket_message(KeyCode.pong, '')]))
        elif current_key_code == KeyCode.pong:
            print("nothing to do just a pong")
        elif current_key_code == KeyCode.to:
            # send a message to a user
            print('sending message to %s' % data)
            send_to = data
            try:
                data_key_code, data_data = read_one_key_code_and_data_from_socket(ac_socket)
                print('new Data %s , %s' % (data_key_code, data_data))
                codes_read += 1
            except InvalidKeyCodeException as e:
                print(e)
                ac_socket.sendall(prepare_final_msg(key, [prepare_socket_message(KeyCode.invalid, '3')]))
                break
            except InvalidKeyDataLengthException as e:
                print(e)
                ac_socket.sendall(prepare_final_msg(key, [prepare_socket_message(KeyCode.invalid, '4')]))
                break
            if data_key_code == KeyCode.data:
                message = data_data
            else:
                ac_socket.sendall(prepare_final_msg(key, [prepare_socket_message(KeyCode.invalid, '5')]))
                break
            to_user_data = get_user_data_from_unique_id(send_to)
            to_user_key = to_user_data.key.encode('utf-8')
            from_user_data = get_user_data_from_key(key)
            if are_friends(from_user_data, to_user_data):
                print(from_user_data, to_user_data, "are friends")
                try:
                    to_socket = active_sockets[to_user_key]
                    print('found socket to send data')
                    msg_1 = prepare_socket_message(KeyCode.message_from, str(from_user_data.id))
                    user_msg = UserMessage()
                    user_msg.from_user = from_user_data
                    user_msg.to_user = to_user_data
                    user_msg.save()
                    msg_id = prepare_socket_message(KeyCode.id, str(user_msg.id))
                    msg_2 = prepare_socket_message(KeyCode.data, message)
                    to_socket.sendall(prepare_final_msg(to_user_data.key, [msg_1, msg_id, msg_2]))
                except KeyError:
                    print('failed to send data, going offline')
                    offline_message_process(to_user_data, message)
            else:
                print(from_user_data, to_user_data, "are not friends")
                ac_socket.sendall(prepare_socket_message(KeyCode.invalid, "6"))
        elif current_key_code == KeyCode.friends:
            # return list of friends
            from_user_data = get_user_data_from_key(key)
            out = []
            for user in get_friends_of_user(from_user_data):
                out_friend = {'id': user.id,
                              'email': user.user.username,
                              'display_name': user.display_name if user.display_name else user.user.first_name}
                out.append(out_friend)
            msg_id = prepare_socket_message(KeyCode.friends, json.dumps(out))
            ac_socket.sendall(prepare_final_msg(key, [msg_id]))
        else:
            ac_socket.sendall(prepare_final_msg(key, [prepare_socket_message(KeyCode.invalid, '7')]))

    selectable_sockets.add(ac_socket)


class ProcessSocket(threading.Thread):
    """
    this is for a new message from a socket
    or processing the socket
    when key is given it assumes key is read and validated
    when key is not given it assumes key has to be read from the socket
    """

    def __init__(self, sock=None, key=None):
        threading.Thread.__init__(self)
        self.c_socket = sock
        self.key = key

    def run(self):
        global selectable_sockets
        if self.key is None:
            valid, self.key = validate_socket(self.c_socket)
            print(valid, self.key)
        else:
            valid = True

        if valid:
            # try:
            process_socket(self.c_socket, self.key)
            # except Exception as e:
            # print("error from process socket")
            # print(e)
        else:
            print("socket with invalid key removing and closing it")
            try:
                selectable_sockets.remove(self.c_socket)
                self.c_socket.close()
            except Exception:
                print("unable to remove socket from process socket. good that it was already removed")


class ProcessNewConnection(threading.Thread):
    """
    this is for a new connection of a socket
    validates key
    adds it to selectable sockets
    and starts process socket on it
    """

    def __init__(self, socket1, ip1, port1):
        threading.Thread.__init__(self)
        self.ac_socket = socket1
        self.ip = ip1
        self.port = port1

    def run(self):
        global active_sockets
        global selectable_sockets
        print("ProcessNewConnection Got new connection from: ", self.ip, self.port)
        valid, key = validate_socket(self.ac_socket)
        if not valid:
            print("invalid new socket")
            print(valid, key)
            self.ac_socket.close()
            print("ProcessNewConnection closed returned 1")
        else:
            active_sockets[key] = self.ac_socket
            thr = ProcessSocket(sock=self.ac_socket, key=key)
            thr.start()


class MainServerConn(threading.Thread):
    def run(self):
        tcp_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        tcp_sock.bind((host, port))
        while True:
            tcp_sock.listen(1000)
            print("\nListening for incoming connections...")
            (client_sock, (cip, c_port)) = tcp_sock.accept()
            new_desk_thread = ProcessNewConnection(client_sock, cip, c_port)
            new_desk_thread.start()


class SelectSockets(threading.Thread):
    """ this thread listens on select with a time out of 1 sec
        if anything is received this responds to those sockets with the
        ProcessSocket socket which computes data and responds
    """

    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        print("SelectSockets")
        while True:
            ready_to_read, ready_to_write, in_error = select.select(selectable_sockets, [], [], 1)
            if len(ready_to_read) > 0:
                print("SelectSockets got ", str(len(ready_to_read)), " to work on")
            for sck in ready_to_read:
                selectable_sockets.remove(sck)
                thr = ProcessSocket(sock=sck)
                thr.start()


def main():
    main_server_conn = MainServerConn()
    main_server_conn.daemon = True
    main_server_conn.start()

    listen_select_sockets = SelectSockets()
    listen_select_sockets.daemon = True
    listen_select_sockets.start()


def print_stats():
    print("Active Threads:", threading.active_count())
    print("No of Sockets:", len(active_sockets))
    print("no of selectable sockets", len(selectable_sockets))


    # yappi.start()
    # funtimes = yappi.get_func_stats()
    # print('call','name','tsub','ttot','tavg')
    # for i in funtimes:
    # if 'woosh_sck' in i.get(1):
    #     print(i.get(0),i.get(1),i.tsub,i.ttot,i.tavg)