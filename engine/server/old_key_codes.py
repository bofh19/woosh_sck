__author__ = 'saikrishnak'


class KeyCode:
    def __init__(self):
        pass

    operation_code = "OPER"
    id = '_id_'
    ok = "OK_C"
    error = "ERRO"
    ping = "PING"
    pong = "PONG"
    data = "DATA"
    to = "TO_C"
    sent = "SENT"
    query = "QUER"
    stats = "STAT"
    data_ttl = "DTTL"
    friends = "FRIE"
    auth_key = "AUTK"
    invite = "INVT"
    invalid = "INVA"
    message_from = "FROM"
    requests_pending_for_you = "REQ1"
    requests_pending_from_you = "REQ2"
    hkey = "HKEY"

    array = [ok, error, ping, pong, data, to, sent, query, stats, data_ttl, friends, invalid, message_from,
             auth_key, invite, requests_pending_for_you, requests_pending_from_you, operation_code, hkey]

    @staticmethod
    def is_valid_key_code(key_code):
        """
        length should return true
        """
        return any(key_code in s for s in KeyCode.array)
