from django.core.exceptions import MultipleObjectsReturned, ObjectDoesNotExist
from django.core.files import File
from django.core.files.temp import NamedTemporaryFile

__author__ = 'saikrishnak'

import threading
import socketserver
from engine.server.interface import validate_key, get_user_data_from_user, make_friends, get_contacts_of_user
from engine.server.interface import force_make_friends, send_request, accept_request, make_user_claimed
from engine.server.interface import get_friends_of_user, get_requests_pending_to_user, are_friends
from engine.server.interface import ignore_on_user_by_user, un_ignore_on_user_by_user, get_ignored_users_by
from engine.server.interface import get_user_data_from_unique_id, get_user_data_from_key
from engine.server.key_code import OperationCode
from engine.models import UserData, UserMessage
import json

host, port = "0.0.0.0", 6070
import socket
import threading
import select
import base64

active_sockets = {}
selectable_sockets = set([])
DATA_LENGTH_FIELD_LENGTH = 10


def validate_socket(ac_socket):
    key = ac_socket.recv(16)
    if not validate_key(key):
        return False, key
    else:
        return True, key


def offline_message_process(user, message):
    pass


def prepare_final_message(client_key, objs):
    if type(client_key) == bytes:
        client_key = client_key.decode('utf-8')
    json_string = json.dumps(objs)
    out_string = base64.b64encode(json_string.encode('utf-8')).decode('utf-8')
    data_length = str(len(out_string))
    if len(data_length) >= 10:
        return (client_key + data_length + out_string).encode('utf-8')
    else:
        for i in range(len(data_length), 10):
            data_length = "0" + data_length
        return (client_key + data_length + out_string).encode('utf-8')


def process_socket(ac_socket, key):
    """
    assumes socket validation has occurred.
    removes from selectable sockets
    and adds it after processing has been done
    :param ac_socket:
    :return:
    """
    global selectable_sockets
    print("process socket called")
    try:
        selectable_sockets.remove(ac_socket)
    except KeyError:
        print("unable to remove socket from selectable_sockets. no idea why")
    try:
        data_length = int(ac_socket.recv(DATA_LENGTH_FIELD_LENGTH))
        print("data_length", data_length)
    except ValueError:
        print("invalid length")
        selectable_sockets.add(ac_socket)

    data = ac_socket.recv(data_length)
    print("actual length: ", data_length, "data recv length: ", len(data))
    while len(data) < data_length:
        left_out = data_length - len(data)
        data = data + ac_socket.recv(left_out)
        # print("after loop actual length: ", data_length, "data recv length: ", len(data))
    data = base64.b64decode(data)
    data = json.loads(data.decode('utf-8'))
    for each_operation in data:
        if each_operation[OperationCode.operation] == OperationCode.ping:
            print('ping')
            out_oper = {OperationCode.operation: OperationCode.pong}
            out_oper = [out_oper]
            ac_socket.sendall(prepare_final_message(key, out_oper))
        elif each_operation[OperationCode.operation] == OperationCode.friends_list:
            print('friends list')
            from_user_data = get_user_data_from_key(key)
            out_friends = []
            for user in get_friends_of_user(from_user_data):
                out_friend = user.to_json()
                out_friend[OperationCode.operation] = OperationCode.friend
                out_friends.append(out_friend)
            out = [{OperationCode.operation: OperationCode.friends_list, OperationCode.friends_list: out_friends}]
            ac_socket.sendall(prepare_final_message(key, out))
        elif each_operation[OperationCode.operation] == OperationCode.contacts_list:
            print('contacts list')
            from_user_data = get_user_data_from_key(key)
            out_contacts = []
            for contact in get_contacts_of_user(from_user_data):
                out_contact = contact.to_json()
                out_contact[OperationCode.operation] = OperationCode.contact
                out_contacts.append(out_contact)
            out = [{OperationCode.operation: OperationCode.contacts_list, OperationCode.contacts_list: out_contacts}]
            ac_socket.sendall(prepare_final_message(key, out))
        elif each_operation[OperationCode.operation] == OperationCode.user_message:
            msg_ack_out = [
                {OperationCode.operation: OperationCode.user_message_received, 'hash': each_operation['hash']}]
            try:
                ac_socket.sendall(prepare_final_message(key, msg_ack_out))
            except KeyError:
                print('failed to send data, going offline')
                offline_message_process(to_user_data, final_message)
            except (socket.timeout, socket.error) as e:
                print("socket or timeout error", e)

            print("send message to a user ", each_operation)
            to_user_data = get_user_data_from_unique_id(each_operation['toId'])
            if to_user_data is None:
                print('invalid')
                out_oper = {OperationCode.operation: OperationCode.invalid}
                out_oper = [out_oper]
                ac_socket.sendall(prepare_final_message(key, out_oper))
                selectable_sockets.add(ac_socket)
                return
            to_user_key = to_user_data.key.encode('utf-8')
            from_user_data = get_user_data_from_key(key)

            user_msg = UserMessage()
            user_msg.from_user = from_user_data
            user_msg.to_user = to_user_data
            user_msg.hkey = each_operation['hash']
            user_msg.save()
            out_oper = {OperationCode.operation: OperationCode.user_message, "fromId": from_user_data.id,
                        "text": each_operation['text'], "toId": to_user_data.id, "hash": each_operation['hash']}
            out_oper = [out_oper]
            final_message = prepare_final_message(to_user_data.key, out_oper)
            try:
                to_socket = active_sockets[to_user_key]
                to_socket.sendall(final_message)
            except KeyError:
                print('failed to send data, going offline')
                offline_message_process(to_user_data, final_message)
            except (socket.timeout, socket.error) as e:
                print("socket or timeout error", e)
                offline_message_process(to_user_data, final_message)
                active_sockets.pop(to_user_key)
                try:
                    selectable_sockets.remove(to_user_key)
                except KeyError:
                    pass
        elif each_operation[OperationCode.operation] == OperationCode.user_message_received:
            print("received user message back reply ", each_operation)
            try:
                user_msg = UserMessage.objects.get(hkey=each_operation['hash'])
                user_msg.received = True
                user_msg.save()
                user_msg_from_user_key = user_msg.from_user.key.encode('utf-8')
                out_operation = {OperationCode.operation: OperationCode.user_message_sent,
                                 "hash": each_operation['hash']}
                final_message = prepare_final_message(user_msg_from_user_key, [out_operation])
                try:
                    sent_socket = active_sockets[user_msg_from_user_key]
                    sent_socket.sendall(final_message)
                    print('sent user_message_sent back to from user', user_msg.from_user)
                except KeyError:
                    print('failed to send data, going offline')
                    offline_message_process(user_msg.from_user, final_message)
                except (socket.timeout, socket.error) as e:
                    print("socket or timeout error", e)
                    offline_message_process(user_msg.from_user, final_message)
                    active_sockets.pop(user_msg_from_user_key)
                    try:
                        selectable_sockets.remove(user_msg_from_user_key)
                    except KeyError:
                        pass
            except (MultipleObjectsReturned, ObjectDoesNotExist):
                print("unable to change message received property ")
        elif each_operation[OperationCode.operation] == OperationCode.profile_pic:
            from_user_data = get_user_data_from_key(key)
            img_temp = NamedTemporaryFile()
            img_temp.write(base64.b64decode(each_operation[OperationCode.profile_pic]))
            img_temp.flush()
            from_user_data.profile_pic.save(each_operation[OperationCode.filename], File(img_temp))
            from_user_profile = from_user_data.to_json()
            from_user_profile[OperationCode.operation] = OperationCode.profile
            out = [{OperationCode.operation: OperationCode.profile_pic}, from_user_profile]
            try:
                ac_socket.sendall(prepare_final_message(key, out))
            except (socket.timeout, socket.error) as e:
                offline_message_process(from_user_data, out)
        elif each_operation[OperationCode.operation] == OperationCode.profile:
            from_user_data = get_user_data_from_key(key)
            from_user_profile = from_user_data.to_json()
            from_user_profile[OperationCode.operation] = OperationCode.profile
            out = [from_user_profile]
            try:
                ac_socket.sendall(prepare_final_message(key, out))
            except (socket.timeout, socket.error) as e:
                offline_message_process(from_user_data, out)
        elif each_operation[OperationCode.operation] == OperationCode.user_status:
            from_user_data = get_user_data_from_key(key)
            from_user_data.user_status = each_operation[OperationCode.user_status]
            from_user_data.save()
            from_user_profile = from_user_data.to_json()
            from_user_profile[OperationCode.operation] = OperationCode.profile
            out = [{OperationCode.operation: OperationCode.user_status}, from_user_profile]
            try:
                ac_socket.sendall(prepare_final_message(key, out))
            except (socket.timeout, socket.error) as e:
                offline_message_process(from_user_data, out)
        elif each_operation[OperationCode.operation] == OperationCode.user_display_name:
            from_user_data = get_user_data_from_key(key)
            from_user_data.display_name = each_operation[OperationCode.user_display_name]
            from_user_data.save()
            from_user_profile = from_user_data.to_json()
            from_user_profile[OperationCode.operation] = OperationCode.profile
            out = [{OperationCode.operation: OperationCode.user_display_name}, from_user_profile]
            try:
                ac_socket.sendall(prepare_final_message(key, out))
            except (socket.timeout, socket.error) as e:
                offline_message_process(from_user_data, out)
        else:
            print("unknown operation code", data)

    selectable_sockets.add(ac_socket)


class ProcessSocket(threading.Thread):
    """
    this is for a new message from a socket
    or processing the socket
    when key is given it assumes key is read and validated
    when key is not given it assumes key has to be read from the socket
    """

    def __init__(self, sock=None, key=None):
        threading.Thread.__init__(self)
        self.c_socket = sock
        self.key = key

    def run(self):
        global selectable_sockets
        if self.key is None:
            valid, self.key = validate_socket(self.c_socket)
            print(valid, self.key)
        else:
            valid = True

        if valid:
            try:
                process_socket(self.c_socket, self.key)
            except Exception as e:
                print("error from process socket", e)
        else:
            print("socket with invalid key removing and closing it")
            try:
                selectable_sockets.remove(self.c_socket)
                self.c_socket.close()
            except Exception:
                print("unable to remove socket from process socket. good that it was already removed")


class ProcessNewConnection(threading.Thread):
    """
    this is for a new connection of a socket
    validates key
    adds it to selectable sockets
    and starts process socket on it
    """

    def __init__(self, socket1, ip1, port1):
        threading.Thread.__init__(self)
        self.ac_socket = socket1
        self.ip = ip1
        self.port = port1

    def run(self):
        global active_sockets
        global selectable_sockets
        print("ProcessNewConnection Got new connection from: e ", self.ip, self.port)
        valid, key = validate_socket(self.ac_socket)
        print("valid ", valid, " key: ", key)
        if not valid:
            print("invalid new socket")
            print(valid, key)
            # self.ac_socket.close() # removed this as sometimes
            # new connections just connect but does not send you any data
            print("ProcessNewConnection closed returned 1")
        else:
            print("valid key for the new socket.")
            active_sockets[key] = self.ac_socket
            process_socket(self.ac_socket, key)


class MainServerConn(threading.Thread):
    def run(self):
        tcp_sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        tcp_sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
        tcp_sock.bind((host, port))
        while True:
            tcp_sock.listen(1000)
            print("\nListening for incoming connections...")
            (client_sock, (cip, c_port)) = tcp_sock.accept()
            new_desk_thread = ProcessNewConnection(client_sock, cip, c_port)
            new_desk_thread.start()


class SelectSockets(threading.Thread):
    """ this thread listens on select with a time out of 1 sec
        if anything is received this responds to those sockets with the
        ProcessSocket socket which computes data and responds
    """

    def __init__(self):
        threading.Thread.__init__(self)

    def run(self):
        print("SelectSockets")
        while True:
            ready_to_read, ready_to_write, in_error = select.select(selectable_sockets, [], [], 1)
            if len(ready_to_read) > 0:
                print("SelectSockets got ", str(len(ready_to_read)), " to work on")
            for sck in ready_to_read:
                selectable_sockets.remove(sck)
                thr = ProcessSocket(sock=sck)
                thr.start()


def main():
    main_server_conn = MainServerConn()
    main_server_conn.daemon = True
    main_server_conn.start()

    listen_select_sockets = SelectSockets()
    listen_select_sockets.daemon = True
    listen_select_sockets.start()


def print_stats():
    print("Active Threads:", threading.active_count())
    print("No of Sockets:", len(active_sockets))
    print("no of selectable sockets", len(selectable_sockets))