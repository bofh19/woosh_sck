__author__ = 'saikrishnak'


class OperationCode:
    def __init__(self):
        pass

    user_message = 'user_message'
    to_user = 'to_user'
    hash_key = 'hash_key'
    ping = 'ping'
    pong = 'pong'
    friends_list = "friends_list"
    operation = "operation"
    friend = "friend"
    invalid = "invalid"
    user_message_received = "user_message_received"
    user_message_sent = "user_message_sent"
    contacts_list = "contacts_list"
    contact = "contact"
    profile_pic = "profile_pic"
    filename = "filename"
    profile = "profile"
    os_type = "os_type"
    os_android = "os_android"
    os_ios = "os_ios"
    os_pc = "os_pc"
    os_web = "os_web"
    os_version = "os_version"
    user_status = "user_status"
    user_display_name = "user_display_name"
    os_desktop = "os_desktop"

    array = [user_message, to_user, hash_key, ping, pong, friends_list, operation, friend, invalid,
             user_message_received, user_message_sent, contacts_list, contact, profile_pic, filename, profile,
             os_type, os_android, os_ios, os_pc, os_web, os_version, user_status, user_display_name, os_desktop]

    @staticmethod
    def is_valid_key_code(operation_code):
        """
        length should return true
        """
        return any(operation_code in s for s in OperationCode.array)
