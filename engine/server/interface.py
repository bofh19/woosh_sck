from django.core.exceptions import ObjectDoesNotExist

__author__ = 'saikrishnak'

from engine.models import UserData, UserObtainedFrom
from engine.models import Relation
from django.contrib.auth.models import User

valid_user_data = {}


def validate_key(key):
    global valid_user_data
    if key not in valid_user_data.keys():
        if UserData.objects.filter(key=key).exists():
            valid_user_data[key] = UserData.objects.get(key=key)
            return valid_user_data[key]
        else:
            return None
    else:
        return valid_user_data[key]


def get_user_data_from_key(key):
    global valid_user_data
    if key not in valid_user_data.keys():
        try:
            valid_user_data[key] = UserData.objects.get(key=key)
            return valid_user_data[key]
        except ObjectDoesNotExist:
            return None
    else:
        return valid_user_data[key]


def get_user_data_from_user(user):
    if UserData.objects.filter(user=user).exists():
        return UserData.objects.get(user=user)
    else:
        return None


def get_user_data_from_unique_id(user_id):
    try:
        return UserData.objects.get(user=User.objects.get(username=user_id))
    except ObjectDoesNotExist:
        try:
            return UserData.objects.get(user__id=user_id)
        except ObjectDoesNotExist:
            return None
    return None


def create_unclaimed_user(username):
    """
    creates a temp unclaimed user in db in temp login
    :param username:
    :return:
    """
    try:
        user = User.objects.create_user(username, email=None, password=UserData.generate_key())
        user.save()
        ud, created = UserData.objects.get_or_create(user=user)
        ud.claimed = False
        ud.save()
        return True
    except Exception:
        return False


def make_user_claimed(user_data):
    """
    makes a user as its claimed
    :param user_data:
    :return:
    """
    user_data.claimed = True
    user_data.save()
    return True


def are_friends(user_data_1, user_data_2):
    """
    return is based on if both user links exist and ignore from both are false
    :param user_data_1:
    :param user_data_2:
    :return:
    """
    if Relation.objects.filter(user1=user_data_1, user2=user_data_2).exists():
        relation = Relation.objects.get(user1=user_data_1, user2=user_data_2)
        if not relation.ignored_by_user1 and not relation.ignored_by_user2 and relation.are_friends:
            return True
    elif Relation.objects.filter(user1=user_data_2, user2=user_data_1).exists():
        relation = Relation.objects.get(user1=user_data_2, user2=user_data_1)
        if not relation.ignored_by_user1 and not relation.ignored_by_user2 and relation.are_friends:
            return True

    return False


def make_friends(user_data_1, user_data_2, accepted=False):
    """
    creates a new relation if does not exist
    does not removes any ignores if exist
    this does not guarantee that both are friends(ignores still apply)
    :param user_data_1:
    :param user_data_2:
    :param accepted:
    :return:
    """
    relation = None
    if Relation.objects.filter(user1=user_data_1, user2=user_data_2).exists():
        relation = Relation.objects.get(user1=user_data_1, user2=user_data_2)
    elif Relation.objects.filter(user1=user_data_2, user2=user_data_1).exists():
        relation = Relation.objects.get(user1=user_data_2, user2=user_data_1)
    else:
        relation, created = Relation.objects.get_or_create(user1=user_data_1, user2=user_data_2)

    relation.are_friends = True
    relation.save()
    return True


def change_ignore_on_user_by_user(on_user, by_user, ignored):
    """
    changes ignore status from user1 to user2
    i.e changing the ignore status being applied by user2
    user2 will now <ignore> user1
    does not change ignore status other way around,
    meaning this does not guarantee that both are friends in anyway(ignore from other still apply)
    :param on_user:
    :param by_user:
    :param ignored:
    :return:
    """
    if Relation.objects.filter(user1=on_user, user2=by_user).exists():
        relation = Relation.objects.get(user1=on_user, user2=by_user)
        relation.ignored_by_user2 = ignored
        relation.save()
    elif Relation.objects.filter(user1=by_user, user2=on_user).exists():
        relation = Relation.objects.get(user1=by_user, user2=on_user)
        relation.ignored_by_user1 = ignored
        relation.save()
    else:
        relation, created = Relation.objects.get_or_create(user1=on_user, user2=by_user)
        relation.ignored_by_user2 = ignored
        relation.save()

    return True


def get_ignored_users_by(user):
    """
    returns users ignored by the user
    :param user:
    :return:
    """
    ignored_friends = []
    relations = Relation.objects.filter(user1=user, ignored_by_user1=True)
    for relation in relations:
        ignored_friends.append(relation.user2)
    relations = Relation.objects.filter(user2=user, ignored_by_user2=True)
    for relation in relations:
        ignored_friends.append(relation.user1)
    return ignored_friends


def ignore_on_user_by_user(on_user, by_user):
    """
    ignores user1 from user2
    i.e user2 now ignores user1
    user2 or user1 cannot send messages to anyone
    :param on_user:
    :param by_user:
    :return:
    """
    return change_ignore_on_user_by_user(on_user, by_user, True)


def un_ignore_on_user_by_user(on_user, by_user):
    """
    removes ignore from user1 to user2
    i.e user2 no longer ignores user1
    ignore other way still applies
    :param on_user:
    :param by_user:
    :return:
    """
    return change_ignore_on_user_by_user(on_user, by_user, False)


def force_make_friends(user_data_1, user_data_2):
    """
    makes both of them friends
    removes ignores from both ways
    :param user_data_1:
    :param user_data_2:
    :return:
    """
    un_ignore_on_user_by_user(user_data_1, user_data_2)
    un_ignore_on_user_by_user(user_data_2, user_data_1)
    make_friends(user_data_1, user_data_2)
    return True


def send_request(from_user, to_user):
    """
    sends request from_user to to_user
    :param from_user:
    :param to_user:
    :return:
    """
    if Relation.objects.filter(user1=from_user, user2=to_user).exists():
        relation = Relation.objects.get(user1=from_user, user2=to_user)
        relation.request_from_user = 1
        relation.save()
    elif Relation.objects.filter(user1=to_user, user2=from_user).exists():
        relation = Relation.objects.get(user1=to_user, user2=from_user)
        relation.request_from_user = 2
        relation.save()
    else:
        relation, created = Relation.objects.get_or_create(user1=from_user, user2=to_user)
        relation.request_from_user = 1
        relation.save()
    return True


def accept_request(from_user, to_user):
    """
    accepts request from_user that was sent to send_user
    or other way and
    makes them friends
    :param from_user:
    :param to_user:
    :return:
    """
    if Relation.objects.filter(user1=from_user, user2=to_user).exists():
        relation = Relation.objects.get(user1=from_user, user2=to_user)
        relation.request_accepted_by_user = True
        relation.save()
        make_friends(from_user, to_user)
        return True
    elif Relation.objects.filter(user1=to_user, user2=from_user).exists():
        relation = Relation.objects.get(user1=to_user, user2=from_user)
        relation.request_accepted_by_user = True
        relation.save()
        make_friends(from_user, to_user)
        return True
    return False


def get_requests_pending_to_user(user):
    """
    lists out all list of UserData that have requested user to be a friend
    :param user:
    :return:
    """
    pending_users = []
    relations = Relation.objects.filter(user2=user, request_accepted_by_user=False, ignored_by_user2=False,
                                        are_friends=False)
    for relation in relations:
        pending_users.append(relation.user1)


def get_friends_of_user(user):
    """
    lists out all this user friends
    :param user:
    :return:
    """
    friends = []
    relations = Relation.objects.filter(user1=user, ignored_by_user1=False, are_friends=True)
    for relation in relations:
        friends.append(relation.user2)
    relations = Relation.objects.filter(user2=user, ignored_by_user2=False, are_friends=True)
    for relation in relations:
        friends.append(relation.user1)
    return friends


def get_contacts_of_user(user):
    """
    lists out all users that are obtained from the given user
    :param user:
    :return:
    """
    contacts = []
    user_obtained_from = UserObtainedFrom.objects.filter(obtained_from=user)
    for u in user_obtained_from:
        contacts.append(u.which_user)
    return contacts